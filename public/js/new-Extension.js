function NewExtension(viewer, options) {
    Autodesk.Viewing.Extension.call(this, viewer, options);
}

NewExtension.prototype = Object.create(Autodesk.Viewing.Extension.prototype);
NewExtension.prototype.constructor = NewExtension;
NewExtension.prototype.load = function() {
    alert('Extension is loaded!');
    var viewer = this.viewer;

    viewer.canvas.addEventListener('dblclick', e=> {
        viewer.setNavigationLock(true);
    });

    var lockBtn = document.getElementById('LockButton');
    lockBtn.addEventListener("click",function() {
        viewer.setNavigationLock(true);
    });

    var unlockBtn = document.getElementById('UnlockButton');
    unlockBtn.addEventListener('click', function() {
        viewer.setNavigationLock(false);
        //var red = new THREE.Vector4(1, 0, 0, 0.5);
        // viewer.setBackgroundColor(1, 0, 0, 0.5);
        //this.onSelectionBinded = this.onSelectionEvent.bind(this);
        //viewer.addEventListener(Autodesk.Viewing.SELECTION_CHANGED_EVENT, this.onSelectionBinded);
    });
    // return true;

    return true;
};


NewExtension.prototype.unload = function() {
    alert('Extension is now unloaded!');
    return true;
};

Autodesk.Viewing.theExtensionManager.registerExtension('New-Extension', NewExtension);