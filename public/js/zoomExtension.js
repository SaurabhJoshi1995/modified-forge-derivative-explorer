var _viewer = MyVars.viewer.getCurrentViewer();
$(_viewer.container).bind("click", onMouseClick);

function onMouseClick(e) {
    var screenPoint = {
        x: event.clientX,
        y: event.clientY
    };

    var n = normalize(screenPoint);
    var dbId = getHitDbId(n.x, n.y);

    if (dbId == null) return;

};

function getHitDbId(x, y) {
    y = 1.0 - y;
    x = x * 2.0 - 1.0;
    y = y * 2.0 - 1.0;

    var vpVec = new THREE.Vector3(x, y, 1);

    var result = _viewer.impl.hitTestViewport(vpVec, false);
    return result ? result.dbId : null;
};

function normalize(screenPoint) {
    var viewport = _viewer.navigator.getScreenViewport();
    var n = {
        x: (screenPoint.x - viewport.left) / viewport.width,
        y: (screenPoint.y - viewport.top) / viewport.height
    };
    return n;
}

_viewer.impl.selector.setSelection([dbId], _viewer.model);
_viewer.fitToView(dbId);
_viewer.isolateById(dbId);