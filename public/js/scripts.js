var MyVars = {
    keepTrying: true
};

$(document).ready(function () {
    //debugger;
    $('#hiddenFrame').attr('src', '');

    // Make sure that "change" event is fired
    // even if same file is selected for upload
    $("#forgeUploadHidden").click(function (evt) {
        evt.target.value = "";
    });

    $("#forgeUploadHidden").change(function(evt) {

        showProgress("Uploading file... ", "inprogress");
        var data = new FormData () ;
        var fileName = this.value;
        data.append (0, this.files[0]) ;
        $.ajax ({
            url: '/dm/files',
            type: 'POST',
            headers: { 'x-file-name': fileName, 'wip-href': MyVars.selectedNode.original.href },
            data: data,
            cache: false,
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            complete: null
        }).done (function (data) {
            console.log('Uploaded file "' + data.fileName + '" with urn = ' + data.objectId);

            // Refresh file tree
            //$('#forgeFiles').jstree("refresh");

            showProgress("Upload successful", "success");
        }).fail (function (xhr, ajaxOptions, thrownError) {
            alert(fileName + ' upload failed!') ;
            showProgress("Upload failed", "failed");
        }) ;
    });

    var upload = $("#uploadFile").click(function(evt) {
        evt.preventDefault();
        $("#forgeUploadHidden").trigger("click");
    });

    // Get the tokens
    get3LegToken(function(token) {
        var auth = $("#authenticate");

        if (!token) {
            auth.click(signIn);
        } else {
            MyVars.token3Leg = token;

            auth.html('You\'re logged in');
            auth.click(function () {
                if (confirm("You're logged in and your token is " + token + '\nWould you like to log out?')) {
                    logoff();
                }
            });

            // Fill the tree with A360 items
            prepareFilesTree();

            // Download list of available file formats
            fillFormats();
        }
    });

    $('#progressInfo').click(function() {
        MyVars.keepTrying = false;
        showProgress("Translation stopped", 'failed');
    });
});

function base64encode(str) {
    var ret = "";
    if (window.btoa) {
        ret = window.btoa(str);
    } else {
        // IE9 support
        ret = window.Base64.encode(str);
    }

    // Remove ending '=' signs
    // Use _ instead of /
    // Use - insteaqd of +
    // Have a look at this page for info on "Unpadded 'base64url' for "named information" URI's (RFC 6920)"
    // which is the format being used by the Model Derivative API
    // https://en.wikipedia.org/wiki/Base64#Variants_summary_table
    var ret2 = ret.replace(/=/g, '').replace(/[/]/g, '_').replace(/[+]/g, '-');

    console.log('base64encode result = ' + ret2);

    return ret2;
}

function signIn() {
    $.ajax({
        url: '/user/authenticate',
        success: function (rootUrl) {
            location.href = rootUrl;
        }
    });
}

function logoff() {
    // Subscribe to the load event to see
    // when the LogOut page got loaded
    $('#hiddenFrame').load(function(event){

        // Unsubscribe from event
        $("#hiddenFrame").off("load");

        // Tell the server to clear session data
        $.ajax({
            url: '/user/logoff',
            success: function (oauthUrl) {
                location.href = oauthUrl;
            }
        });
    });

    // Load the LogOut page
    $('#hiddenFrame').attr('src', 'https://accounts.autodesk.com/Authentication/LogOut');
}

// function areaToolbar(viewer){
//     //var view = Autodesk.Viewing.Viewer3D()
//     // var config = {
//     //     extensions: ['Autodesk.Viewing.WebVR', 'Autodesk.Viewing.MarkupsGui'],
//     //     experimental: ['webVR_orbitModel']
//     // };
//     // var view = new Autodesk.Viewing.Private.GuiViewer3D(viewerElement, config);
//     var btn = $(#toolbar);
//     btn.onclick(function (viewer) {
//         // MyVars.viewer.applyCamera(Three.Camera);
//         // MyVars.viewer.getCamera();
//     });
// }
function get3LegToken(callback) {

    if (callback) {
        $.ajax({
            url: '/user/token',
            success: function (data) {
                MyVars.token3Leg = data.token;
                console.log('Returning new 3 legged token (User Authorization): ' + MyVars.token3Leg);
                callback(data.token, data.expires_in);
            }
        });
    } else {
        console.log('Returning saved 3 legged token (User Authorization): ' + MyVars.token3Leg);

        return MyVars.token3Leg;
    }
}

// http://stackoverflow.com/questions/4068373/center-a-popup-window-on-screen
function PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}

function downloadDerivative(urn, derUrn, fileName) {
    console.log("downloadDerivative for urn=" + urn + " and derUrn=" + derUrn);
    // fileName = file name you want to use for download
    var url = window.location.protocol + "//" + window.location.host +
        "/md/download?urn=" + urn +
        "&derUrn=" + derUrn +
        "&fileName=" + encodeURIComponent(fileName);

    window.open(url,'_blank');
}

function getThumbnail(urn) {
    console.log("downloadDerivative for urn=" + urn);
    // fileName = file name you want to use for download
    var url = window.location.protocol + "//" + window.location.host +
        "/dm/thumbnail?urn=" + urn;

    window.open(url,'_blank');
}

function isArraySame(arr1, arr2) {
    // If both are undefined or has no value
    if (!arr1 && !arr2)
        return true;

    // If just one of them has no value
    if (!arr1 || !arr2)
        return false;

    return (arr1.sort().join(',') === arr2.sort().join(','));
}

function getDerivativeUrns(derivative, format, getThumbnail, objectIds) {
    console.log(
        "getDerivativeUrns for derivative=" + derivative.outputType +
        " and objectIds=" + (objectIds ? objectIds.toString() : "none"));
    var res = [];
    for (var childId in derivative.children) {
        var child = derivative.children[childId];
        // using toLowerCase to handle inconsistency
        if (child.role === '3d' || child.role.toLowerCase() === format) {
            if (isArraySame(child.objectIds, objectIds)) {
                // Some formats like svf might have children
                if (child.children) {
                    for (var subChildId in child.children) {
                        var subChild = child.children[subChildId];

                        if (subChild.role === 'graphics') {
                            res.push(subChild.urn);
                            if (!getThumbnail)
                                return res;
                        } else if (getThumbnail && subChild.role === 'thumbnail') {
                            res.push(subChild.urn);
                            return res;
                        }
                    }
                } else {
                    res.push(child.urn);
                    return res;
                }
            }
        }
    }

    return null;
}

// OBJ: guid & objectIds are also needed
// SVF, STEP, STL, IGES:
// Posts the job then waits for the manifest and then download the file
// if it's created
function askForFileType(format, urn, guid, objectIds, rootFileName, fileExtType, onsuccess) {
    console.log("askForFileType " + format + " for urn=" + urn);
    var advancedOptions = {
        'stl' : {
            "format": "binary",
            "exportColor": true,
            "exportFileStructure": "single" // "multiple" does not work
        },
        'obj' : {
            "modelGuid": guid,
            "objectIds": objectIds
        }
    };

    $.ajax({
        url: '/md/export',
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(
            {
                urn: urn,
                format: format,
                advanced: advancedOptions[format],
                rootFileName: rootFileName,
                fileExtType: fileExtType
            }
        )
    }).done(function (data) {
        console.log(data);

        if (data.result === 'success' // newly submitted data
            || data.result === 'created') { // already submitted data
            getManifest(urn, function (res) {
                onsuccess(res);
            });
        }
    }).fail(function(err) {
        showProgress("Could not start translation", "fail");
        console.log('/md/export call failed\n' + err.statusText);
    });
}

// We need this in order to get an OBJ file for the model
function getMetadata(urn, onsuccess) {
    console.log("getMetadata for urn=" + urn);
    $.ajax({
        url: '/md/metadatas/' + urn,
        type: 'GET'
    }).done(function (data) {
        console.log(data);

        // Get first model guid
        // If it does not exists then something is wrong
        // let's check the manifest
        // If get manifest sees a failed attempt then it will
        // delete the manifest
        var md0 = data.data.metadata[0];
        if (!md0) {
            getManifest(urn, function () {});
        } else {
            var guid = md0.guid;
            if (onsuccess !== undefined) {
                onsuccess(guid);
            }
        }
    }).fail(function(err) {
        console.log('GET /md/metadata call failed\n' + err.statusText);
    });
}

function getHierarchy(urn, guid, onsuccess) {
    console.log("getHierarchy for urn=" + urn + " and guid=" + guid);
    $.ajax({
        url: '/md/hierarchy',
        type: 'GET',
        data: {urn: urn, guid: guid}
    }).done(function (data) {
        console.log(data);

        // If it's 'accepted' then it's not ready yet
        if (data.result === 'accepted') {
            // Let's try again
            if (MyVars.keepTrying) {
                //window.setTimeout(function() {
                initializeViewer(urn);
                        //getHierarchy(urn, guid, onsuccess);
                    //}, 500
                //);
            } else {
                MyVars.keepTrying = true;
            }

            return;
        }

        // We got what we want
        if (onsuccess !== undefined) {
            onsuccess(data);
        }
    }).fail(function(err) {
        console.log('GET /md/hierarchy call failed\n' + err.statusText);
    });
}

function getProperties(urn, guid, onsuccess) {
    console.log("getProperties for urn=" + urn + " and guid=" + guid);
    $.ajax({
        url: '/md/properties',
        type: 'GET',
        data: {urn: urn, guid: guid}
    }).done(function (data) {
        console.log(data);

        if (onsuccess !== undefined) {
            onsuccess(data);
        }
    }).fail(function(err) {
        console.log('GET /api/properties call failed\n' + err.statusText);
    });
}

function getManifest(urn, onsuccess) {
    console.log("getManifest for urn=" + urn);
    $.ajax({
        url: '/md/manifests/' + urn,
        type: 'GET'
    }).done(function (data) {
        console.log(data);

        if (data.status !== 'failed') {
            if (data.progress !== 'complete') {
                showProgress("Translation progress: " + data.progress, data.status);

                if (MyVars.keepTrying) {
                    // Keep calling until it's done
                    window.setTimeout(function() {
                            getManifest(urn, onsuccess);
                        }, 500
                    );
                } else {
                    MyVars.keepTrying = true;
                }
            } else {
                showProgress("Translation completed", data.status);
                onsuccess(data);
            }
        // if it's a failed translation best thing is to delete it
        } else {
            showProgress("Translation failed", data.status);
            // Should we do automatic manifest deletion in case of a failed one?
            //delManifest(urn, function () {});
        }
    }).fail(function(err) {
        showProgress("Translation failed", 'failed');
        console.log('GET /api/manifest call failed\n' + err.statusText);
    });
}

function delManifest(urn, onsuccess) {
    console.log("delManifest for urn=" + urn);
    $.ajax({
        url: '/md/manifests/' + urn,
        type: 'DELETE'
    }).done(function (data) {
        console.log(data);
        if (data.status === 'success') {
            if (onsuccess !== undefined) {
                onsuccess(data);
            }
        }
    }).fail(function(err) {
        console.log('DELETE /api/manifest call failed\n' + err.statusText);
    });
}

function addColorManifest(urn, onsuccess) {
//document.getElementById('forgeViewer').style.backgroundColor = "red"
    jQuery(document).ready(function() {
        $("button").on("click", "#addColorManifest", function() {
            $('#forgeViewer').css("background-color", "#9683ec");
        });

        // $("button").on("click", "#blue", function() {
        //     $('body').css("background-color", "#00bfff");
        // });
    });
}

/////////////////////////////////////////////////////////////////
// Formats / #forgeFormats
// Shows the export file formats available for the selected model
/////////////////////////////////////////////////////////////////

function getFormats(onsuccess) {
    console.log("getFormats");
    $.ajax({
        url: '/md/formats',
        type: 'GET'
    }).done(function (data) {
        console.log(data);

        if (onsuccess !== undefined) {
            onsuccess(data);
        }
    }).fail(function(err) {
        console.log('GET /md/formats call failed\n' + err.statusText);
    });
}

function fillFormats() {
    getFormats(function(data) {
        var forgeFormats = $("#forgeFormats");
        forgeFormats.data("forgeFormats", data);

        var download = $("#downloadExport");
        download.click(function() {
            MyVars.keepTrying = true;

            var elem = $("#forgeHierarchy");
            var tree = elem.jstree();
            var rootNodeId = tree.get_node('#').children[0];
            var rootNode = tree.get_node(rootNodeId);

            var format = $("#forgeFormats").val();
            var urn = MyVars.selectedUrn;
            var guid = MyVars.selectedGuid;
            var fileName = rootNode.text + "." + format;
            var rootFileName = MyVars.rootFileName;
            var nodeIds = elem.jstree("get_checked", null, true);

            // Only OBJ supports subcomponent selection
            // using objectId's
            var objectIds = null;
            if (format === 'obj') {
                objectIds = [-1];
                if (nodeIds.length) {
                    objectIds = [];

                    $.each(nodeIds, function (index, value) {
                        objectIds.push(parseInt(value, 10));
                    });
                }
            }

            // The rest can be exported with a single function
            askForFileType(format, urn, guid, objectIds, rootFileName, MyVars.fileExtType, function (res) {
                if (format === 'thumbnail') {
                    getThumbnail(urn);

                    return;
                }

                // Find the appropriate obj part
                for (var derId in res.derivatives) {
                    var der = res.derivatives[derId];
                    if (der.outputType === format) {
                        // found it, now get derivative urn
                        // leave objectIds parameter undefined
                        var derUrns = getDerivativeUrns(der, format, false, objectIds);

                        // url encode it
                        if (derUrns) {
                            derUrns[0] = encodeURIComponent(derUrns[0]);

                            downloadDerivative(urn, derUrns[0], fileName);

                            // in case of obj format, also try to download the material
                            if (format === 'obj') {
                                // The MTL file needs to have the exact name that it has on OSS
                                // because that's how it's referenced from the OBJ file
                                var ossName = decodeURIComponent(derUrns[0]);
                                var ossNameParts = ossName.split("/");
                                // Get the last element
                                ossName = ossNameParts[ossNameParts.length - 1];

                                downloadDerivative(urn, derUrns[0].replace('.obj' , '.mtl'), ossName.replace('.obj', '.mtl'));
                            }
                        } else {
                            showProgress("Could not find specific OBJ file", "failed");
                            console.log("askForFileType, Did not find the OBJ translation with the correct list of objectIds");
                        }

                        return;
                    }
                }

                showProgress("Could not find exported file", "failed");
                console.log("askForFileType, Did not find " + format + " in the manifest");
            });

        });

        var deleteManifest = $("#deleteManifest");
        deleteManifest.click(function() {
            var urn = MyVars.selectedUrn;

            cleanupViewer();

            delManifest(urn, function() { });
        });
    });
}

jQuery(document).ready(function() {
    $("div").on("click", "#addColorManifest", function() {
        $('#forgeViewer1').css("background-color", "#9683ec");
    });

    $("button").on("click", "#blue", function() {
        $('body').css("background-color", "#00bfff");
    });
});
function updateFormats(format) {

    var forgeFormats = $("#forgeFormats");
    var formats = forgeFormats.data("forgeFormats");
    forgeFormats.empty();

    // obj is not listed for all possible files
    // using this workaround for the time being
    //forgeFormats.append($("<option />").val('obj').text('obj'));

    $.each(formats.formats, function(key, value) {
        if (key === 'obj' || value.indexOf(format) > -1) {
            forgeFormats.append($("<option />").val(key).text(key));
        }
    });
}

/////////////////////////////////////////////////////////////////
// Files Tree / #forgeFiles
// Shows the A360 hubs, projects, folders and files of
// the logged in user
/////////////////////////////////////////////////////////////////

var haveBIM360Hub = false;

function prepareFilesTree() {
    console.log("prepareFilesTree");
    $.getJSON("/api/forge/clientID", function (res) {
        $("#ClientID").val(res.ForgeClientId);
    });

    $('#forgeFiles').jstree({
        'core': {
            'themes': {"icons": true},
            'check_callback': true, // make it modifiable
            'data': {
                cache: false,
                "url": '/dm/treeNode',
                "dataType": "json",
                "data": function (node) {
                    return {
                        "href": (node.id === '#' ? '#' : node.original.href)
                    };
                },
                "success": function (nodes) {
                    nodes.forEach(function (n) {
                        if (n.type === 'hubs' && n.href.indexOf('b.') > 0)
                            haveBIM360Hub = true;
                    });

                    if (!haveBIM360Hub) {
                        $("#provisionAccountModal").modal();
                        $("#provisionAccountSave").click(function () {
                            $('#provisionAccountModal').modal('toggle');
                            $('#forgeFiles').jstree(true).refresh();
                        });
                        haveBIM360Hub = true;
                    }
                }
            }
        },
        'types': {
            'default': {
                'icon': 'glyphicon glyphicon-cloud'
            },
            '#': {
                'icon': 'glyphicon glyphicon-user'
            },
            'hubs': {
                'icon': 'glyphicon glyphicon-inbox'
            },
            'projects': {
                'icon': 'glyphicon glyphicon-list-alt'
            },
            'items': {
                'icon': 'glyphicon glyphicon-briefcase'
            },
            'folders': {
                'icon': 'glyphicon glyphicon-folder-open'
            },
            'versions': {
                'icon': 'glyphicon glyphicon-time'
            }
        },
        "plugins": ["types", "contextmenu"], // let's not use sort or state: , "state" and "sort"],
        'contextmenu': {
            'select_node': false,
            'items': filesTreeContextMenu
        }
    }).bind("select_node.jstree", function (evt, data) {
        // Clean up previous instance
        cleanupViewer();

        // Disable the hierarchy related controls for the time being
        $("#forgeFormats").attr('disabled', 'disabled');
        $("#downloadExport").attr('disabled', 'disabled');

        if (data.node.type === 'versions') {
            $("#deleteManifest").removeAttr('disabled');
            $("#uploadFile").removeAttr('disabled');

            MyVars.keepTrying = true;
            MyVars.selectedNode = data.node;

            // Clear hierarchy tree
            $('#forgeHierarchy').empty().jstree('destroy');

            // Clear properties tree
            $('#forgeProperties').empty().jstree('destroy');

            // Delete cached data
            $('#forgeProperties').data('forgeProperties', null);

            updateFormats(data.node.original.fileType);

            // Store info on selected file
            MyVars.rootFileName = data.node.original.rootFileName;
            MyVars.fileName = data.node.original.fileName;
            MyVars.fileExtType = data.node.original.fileExtType;

            if ($('#wipVsStorage').hasClass('active')) {
                console.log("Using WIP id");
                MyVars.selectedUrn = base64encode(data.node.original.wipid);
            } else {
                console.log("Using Storage id");
                MyVars.selectedUrn = base64encode(data.node.original.storage);
            }

            // Fill hierarchy tree
            // format, urn, guid, objectIds, rootFileName, fileExtType
            showHierarchy(
                MyVars.selectedUrn,
                null,
                null,
                MyVars.rootFileName,
                MyVars.fileExtType
            );
            console.log(
                "data.node.original.storage = " + data.node.original.storage,
                "data.node.original.wipid = " + data.node.original.wipid,
                ", data.node.original.fileName = " + data.node.original.fileName,
                ", data.node.original.fileExtType = " + data.node.original.fileExtType
            );

            // Show in viewer
            //initializeViewer(data.node.data);
        } else {
            $("#deleteManifest").attr('disabled', 'disabled');
            $("#uploadFile").attr('disabled', 'disabled');

            // Just open the children of the node, so that it's easier
            // to find the actual versions
            $("#forgeFiles").jstree("open_node", data.node);

            // And clear trees to avoid confusion thinking that the
            // data belongs to the clicked model
            $('#forgeHierarchy').empty().jstree('destroy');
            $('#forgeProperties').empty().jstree('destroy');
        }
    });
}

function downloadAttachment(href, attachmentVersionId) {
    console.log("downloadAttachment for href=" + href);
    // fileName = file name you want to use for download
    var url = window.location.protocol + "//" + window.location.host +
        "/dm/attachments/" + encodeURIComponent(attachmentVersionId) + "?href=" + encodeURIComponent(href);

    window.open(url,'_blank');
}

function downloadFile(href) {
    console.log("downloadFile for href=" + href);
    // fileName = file name you want to use for download
    var url = window.location.protocol + "//" + window.location.host +
        "/dm/files/" + encodeURIComponent(href);

    window.open(url,'_blank');
}

function deleteAttachment(href, attachmentVersionId) {
    alert("Functionality not available yet");
    return;

    console.log("deleteAttachment for href=" + href);
    $.ajax({
        url: '/dm/attachments/' + encodeURIComponent(attachmentVersionId),
        headers: { 'wip-href': href },
        type: 'DELETE'
    }).done(function (data) {
        console.log(data);
        if (data.status === 'success') {
            if (onsuccess !== undefined) {
                onsuccess(data);
            }
        }
    }).fail(function(err) {
        console.log('DELETE /api/manifest call failed\n' + err.statusText);
    });
}

function filesTreeContextMenu(node, callback) {
    if (node.type === 'versions') {
        $.ajax({
            url: '/dm/attachments',
            data: {href: node.original.href},
            type: 'GET',
            success: function (data) {
                var menuItems = {};
                menuItems["download"] = {
                    "label": "Download",
                    "action": function (obj) {
                        downloadFile(obj.item.href);
                    },
                    "href": node.original.href
                };
                data.data.forEach(function (item) {
                    if (item.meta.extension.type === "auxiliary:autodesk.core:Attachment") {
                        var menuItem = {
                            "label": item.displayName,
                            "action": function (obj) {
                                alert(obj.item.label + " with versionId = " + obj.item.versionId);
                            },
                            "versionId": item.id,
                            "submenu" : {
                                "open": {
                                    "label": "Open",
                                    "action": function (obj) {
                                        downloadAttachment(obj.item.href, obj.item.versionId);
                                    },
                                    "versionId": item.id,
                                    "href": node.original.href
                                },
                                "delete": {
                                    "label": "Delete",
                                    "action": function (obj) {
                                        deleteAttachment(obj.item.href, obj.item.versionId);
                                    },
                                    "versionId": item.id,
                                    "href": node.original.href
                                }
                            }
                        };

                        menuItems = menuItems || {};
                        menuItems[item.id] = menuItem;
                    }
                })

                if (Object.keys(menuItems).length < 2) {
                    menuItems["noItem"] = {
                        "label": "No attachments", 
                        "action": function () {}
                    };
                } 
                    
                callback(menuItems);
            }
        });
    }

    return;
}

/////////////////////////////////////////////////////////////////
// Hierarchy Tree / #forgeHierarchy
// Shows the hierarchy of components in selected model
/////////////////////////////////////////////////////////////////

function showHierarchy(urn, guid, objectIds, rootFileName, fileExtType) {

    // You need to
    // 1) Post a job
    // 2) Get matadata (find the model guid you need)
    // 3) Get the hierarchy based on the urn and model guid

    // Get svf export in order to get hierarchy and properties
    // for the model
    var format = 'svf';
    askForFileType(format, urn, guid, objectIds, rootFileName, fileExtType, function (manifest) {
        getMetadata(urn, function (guid) {
            showProgress("Retrieving hierarchy...", "inprogress");

            getHierarchy(urn, guid, function (data) {
                showProgress("Retrieved hierarchy", "success");

                for (var derId in manifest.derivatives) {
                    var der = manifest.derivatives[derId];
                    // We just have to make sure there is an svf
                    // translation, but the viewer will find it
                    // from the urn
                    if (der.outputType === 'svf') {

                        initializeViewer(urn);
                    }
                }

                prepareHierarchyTree(urn, guid, data.data);
            });
        });
    });
}

function addHierarchy(nodes) {
    for (var nodeId in nodes) {
        var node = nodes[nodeId];

        // We are also adding properties below that
        // this function might iterate over and we should skip
        // those nodes
        if (node.type && node.type === 'property' || node.type === 'properties') {
            // skip this node
            var str = "";
        } else {
            node.text = node.name;
            node.children = node.objects;
            if (node.objectid === undefined) {
                node.type = 'dunno'
            } else {
                node.id = node.objectid;
                node.type = 'object'
            }
            addHierarchy(node.objects);
        }
    }
}

function prepareHierarchyTree(urn, guid, json) {
    // Convert data to expected format
    addHierarchy(json.objects);

    // Enable the hierarchy related controls
    $("#forgeFormats").removeAttr('disabled');
    $("#downloadExport").removeAttr('disabled');

    // Store info of selected item
    MyVars.selectedUrn = urn;
    MyVars.selectedGuid = guid;

    // init the tree
    $('#forgeHierarchy').jstree({
        'core': {
            'check_callback': true,
            'themes': {"icons": true},
            'data': json.objects
        },
        'checkbox' : {
            'tie_selection': false,
            "three_state": true,
            'whole_node': false
        },
        'types': {
            'default': {
                'icon': 'glyphicon glyphicon-cloud'
            },
            'object': {
                'icon': 'glyphicon glyphicon-save-file'
            }
        },
        "plugins": ["types", "sort", "checkbox", "ui", "themes", "contextmenu"],
        'contextmenu': {
            'select_node': false,
            'items': hierarchyTreeContextMenu
        }
    }).bind("select_node.jstree", function (evt, data) {
        if (data.node.type === 'object') {
            var urn = MyVars.selectedUrn;
            var guid = MyVars.selectedGuid;
            var objectId = data.node.original.objectid;

            // Empty the property tree
            $('#forgeProperties').empty().jstree('destroy');

            fetchProperties(urn, guid, function (props) {
                preparePropertyTree(urn, guid, objectId, props);
                selectInViewer([objectId]);
            });
        }
    }).bind("check_node.jstree uncheck_node.jstree", function (evt, data) {
        // To avoid recursion we are checking if the changes are
        // caused by a viewer selection which is calling
        // selectInHierarchyTree()
        if (!MyVars.selectingInHierarchyTree) {
            var elem = $('#forgeHierarchy');
            var nodeIds = elem.jstree("get_checked", null, true);

            // Convert from strings to numbers
            var objectIds = [];
            $.each(nodeIds, function (index, value) {
                objectIds.push(parseInt(value, 10));
            });

            selectInViewer(objectIds);
        }
    });
}

function selectInHierarchyTree(objectIds) {
    MyVars.selectingInHierarchyTree = true;

    var tree = $("#forgeHierarchy").jstree();

    // First remove all the selection
    //tree.uncheck_all();
    //  tree.uncheck_all();
    // Now select the newly selected items
    for (var key in objectIds) {
        var id = objectIds[key];

        // Select the node
        //tree.check_node(id);

        // Make sure that it is visible for the user
        tree._open_to(id);
    }

    MyVars.selectingInHierarchyTree = false;
}

function hierarchyTreeContextMenu(node, callback) {
    var menuItems = {};

    var menuItem = {
        "label": "Select in Fusion",
        "action": function (obj) {
            var path = $("#forgeHierarchy").jstree().get_path(node,'/');
            alert(path);

            // Open this in the browser:
            // fusion360://command=open&file=something&properties=MyCustomPropertyValues
            var url = "fusion360://command=open&file=something&properties=" + encodeURIComponent(path);
            $("#fusionLoader").attr("src", url);
            $("#areafusionLoader").attr("src", url);
            $('#toolbarFusionLoader').attr("src", url);
        }
    };
    menuItems[0] = menuItem;

    //callback(menuItems);

    //return menuItems;
    return null; // for the time being
}

/////////////////////////////////////////////////////////////////
// Property Tree / #forgeProperties
// Shows the properties of the selected sub-component
/////////////////////////////////////////////////////////////////

// Storing the collected properties since you get them for the whole
// model. So when clicking on the various sub-components in the
// hierarchy tree we can reuse it instead of sending out another
// http request
function fetchProperties(urn, guid, onsuccess) {
    var props = $("#forgeProperties").data("forgeProperties");
    if (!props) {
        getProperties(urn, guid, function(data) {
            $("#forgeProperties").data("forgeProperties", data.data);
            onsuccess(data.data);
        })
    } else {
        onsuccess(props);
    }
}

// Recursively add all the additional properties under each
// property node
function addSubProperties(node, props) {
    node.children = node.children || [];
    for (var subPropId in props) {
        var subProp = props[subPropId];
        if (subProp instanceof Object) {
            var length = node.children.push({
                "text": subPropId,
                "type": "properties"
            });
            var newNode = node.children[length-1];
            addSubProperties(newNode, subProp);
        } else {
            node.children.push({
                "text": subPropId + " = " + subProp.toString(),
                "type": "property"
            });
        }
    }
}

// Add all the properties of the selected sub-component
function addProperties(node, props) {
    // Find the relevant property section
    for (var propId in props) {
        var prop = props[propId];
        if (prop.objectid === node.objectid) {
            addSubProperties(node, prop.properties);
        }
    }
}

function preparePropertyTree(urn, guid, objectId, props) {
    // Convert data to expected format
    var data = { 'objectid' : objectId };
    addProperties(data, props.collection);

    // init the tree
    $('#forgeProperties').jstree({
        'core': {
            'check_callback': true,
            'themes': {"icons": true},
            'data': data.children
        },
        'types': {
            'default': {
                'icon': 'glyphicon glyphicon-cloud'
            },
            'property': {
                'icon': 'glyphicon glyphicon-tag'
            },
            'properties': {
                'icon': 'glyphicon glyphicon-folder-open'
            }
        },
        "plugins": ["types", "sort"]
    }).bind("activate_node.jstree", function (evt, data) {
       //
    });
}

/////////////////////////////////////////////////////////////////
// Viewer
// Based on Autodesk Viewer basic sample
// https://developer.autodesk.com/api/viewerapi/
/////////////////////////////////////////////////////////////////

function cleanupViewer() {
    // Clean up previous instance
    if (MyVars.viewer && MyVars.viewer.model) {
        console.log("Unloading current model from Autodesk Viewer");

        //MyVars.viewer.impl.unloadModel(MyVars.viewer.model);
        //MyVars.viewer.impl.sceneUpdated(true);
        MyVars.viewer.tearDown();
        MyVars.viewer.setUp(MyVars.viewer.config);

        document.getElementById('forgeViewer').style.display = 'none';
    }
}

function initializeViewer(urn) {
    cleanupViewer();
    //this.urn_model1 ="dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6d2lwLmRtLnByb2QvYzc4MDhhNmMtN2MwZi00NzA5LTk2NzEtYTY5YjM4YmI1ZGEwLmR3Zw";
    this.urn_model1 ="house_3.dwg";
    //this.urn_model2 ="dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6d2lwLmRtLnByb2QvOTEyYTAxYjItZmI5ZS00MTM0LWJiNjYtMmJjNmJjMTM0NjM3LmR3Zw";
    this.urn_model2 ="football_stadium_.dwg";

//model info array
    this.modelArray = [	{modelName:'urn_model1',urn:urn_model1,modelObj:null},
        {modelName:'urn_model2',urn:urn_model2,modelObj:null}]

    document.getElementById('forgeViewer').style.display = 'block';

    console.log("Launching Autodesk Viewer for: " + urn);

    var options = {
        document: 'urn:' + urn,
        env: 'AutodeskProduction',
        getAccessToken: get3LegToken // this works fine, but if I pass get3LegToken it only works the first time
    };

    if (MyVars.viewer) {
        loadDocument(MyVars.viewer, options.document);
    } else {
        var viewerElement = document.getElementById('forgeViewer');
        var config = {
            extensions: ['Autodesk.Viewing.WebVR', 'Autodesk.Viewing.MarkupsGui','New-Extension','simple-panel',
                'MyNewExtension','Autodesk.Viewing.MarkupsCore','Autodesk.Measure','Autodesk.DocumentBrowser','Autodesk.BIM360.RollCamera','Autodesk.Viewing.Extensions.Measure.MeasureTool'],
            experimental: ['webVR_orbitModel']
        };
        MyVars.viewer = new Autodesk.Viewing.Private.GuiViewer3D(viewerElement, config);
        //MyVars.app = new Autodesk.Viewing.ViewingApplication(viewerElement);
        //MyVars.viewer = new Autodesk.Viewing.Viewer3D(viewerElement, config);
        Autodesk.Viewing.Initializer(
            options,
            function () {
                globalPromise(modelArray);
                MyVars.viewer.start();// this would be needed if we also want to load extensions
                MyVars.viewer.getSelectionCount();
                MyVars.viewer.loadExtension('Autodesk.Measure');
                MyVars.viewer.loadExtension('Autodesk.Viewing.ZoomWindow');
                MyVars.viewer.loadExtension('Autodesk.LayerManager');
                MyVars.viewer.loadExtension('Autodesk.DocumentBrowser');
                MyVars.viewer.loadExtension('Autodesk.DefaultTools.NavTools');
                //MyVars.viewer.loadExtension('Autodesk.Viewing.ViewingUtilities');
                // MyVars.viewer.loadExtension('Autodesk.PDF').then( () => {
                //     MyVars.viewer.loadModel( urn, MyVars.viewer);
                //     MyVars.viewer.loadExtension("Autodesk.Viewing.MarkupsCore");
                //     MyVars.viewer.loadExtension("Autodesk.Viewing.MarkupsGui");
                // });
                //MyVars.viewer.Autodesk.Viewing.Document.getSubItemsWithProperties();
                loadDocument(MyVars.viewer, options.document);
                addSelectionListener(MyVars.viewer);
                console.log("Hello");
            }
        );
    }
}
globalPromise = (modelArray)=> {

    var _this = this;

    //each promise function
    //input the index of model array

    function promiseEachModel(index){
        return new Promise((resolve, reject)=>
        {
            var modelName = modelArray[index].modelName;
            var _index = index;

            //when the document is loaded
            function _onDocumentLoadSuccess(doc) {
                console.log(modelName +
                    ': Document Load Succeeded!');
                _this.globalDocumentLoad(doc,
                    _onLoadModelSuccess,
                    _onLoadModelError);
            };

            //when the document failed to be loaded
            function _onDocumentLoadFailure(viewerErrorCode) {
                console.error(modelName +
                    ': Document Load Failure, errorCode:' +
                    viewerErrorCode);
            }

            //when the model is loaded
            function _onLoadModelSuccess(model) {
                console.log(modelName + ': Load Model Succeeded!');

                //delegate geometry loaded event
                MyVars.viewer.addEventListener(
                    Autodesk.Viewing.GEOMETRY_LOADED_EVENT,
                    _onGeometryLoaded);

                //map this item with the corresponding model in case of use
                modelArray[index].modelObj = model
            };

            function _onLoadModelError(viewerErrorCode) {
                console.error(modelName +
                    ': Load Model Error, errorCode:' +
                    viewerErrorCode);
                //any error
                reject(modelName + ' Loading Failed!' + viewerErrorCode);
            }

            function _onGeometryLoaded(evt){
                //_this.globalGeometryLoaded(modelName,evt.model);
                MyVars.viewer.removeEventListener(
                    Autodesk.Viewing.GEOMETRY_LOADED_EVENT,
                    _onGeometryLoaded);
                console.log(modelName + '  Geometry Loaded!');
                resolve(modelName + '  Geometry Loaded!');
            }


            //load the model
            Autodesk.Viewing.Document.load(
                modelArray[index].urn,
                _onDocumentLoadSuccess,
                _onDocumentLoadFailure );

        }); //end of new promise

    }//end of each promise function

    //build the index array
    var indexArr = [0,1,2,3];

    //proces each promise
    //refer to http://jsfiddle.net/jfriend00/h3zaw8u8/
    function processArray(array, fn) {
        var results = [];
        return array.reduce(function(p, item) {
            return p.then(function() {
                return fn(item).then(function(data) {
                    results.push(data);
                    return results;
                });
            });
        }, Promise.resolve());
    }

    //start to process
    processArray(indexArr, promiseEachModel).then(function(result) {
        console.log(result);
    }, function(reason) {
        console.log(reason);
    });
}//end of function globalPromise


//when document is being loaded
globalDocumentLoad = (doc,_onLoadModelSuccess,_onLoadModelError)=>
{
    //get available viewables
    var viewables = Autodesk.Viewing.Document.getSubItemsWithProperties(
        doc.getRootItem(), {'type':'geometry'}, true);
    if (viewables.length === 0) {
        console.error('Document contains no viewables.');
        return;
    }

    // Choose the first avialble viewables
    var initialViewable = viewables[0];
    var svfUrl = doc.getViewablePath(initialViewable);

    var mat = new THREE.Matrix4();
    //input the transformation
    var loadOptions = {
        placementTransform: mat ,
        globalOffset:{x:0,y:0,z:0}, // to align the models
        sharedPropertyDbPath: doc.getPropertyDbPath()
    };

    //if this is the first model
    if(doc.myPath == this.urn_model1){

        //load the first model
        MyVars.viewer.start(svfUrl,
            loadOptions,
            _onLoadModelSuccess,
            _onLoadModelError);

    }
    else{
        //other models
        MyVars.viewer.loadModel(svfUrl,
            loadOptions,
            _onLoadModelSuccess,
            _onLoadModelError);
    }
}
function activatemarkupToolbar(){
    var extension = MyVars.viewer.getExtension("Autodesk.Viewing.MarkupsCore");
    extension.enterEditMode();
}

function markupToolbar(){
    //MyVars.viewer.loadExtension(Autodesk.Viewing.ZoomWindow);
    var extension = MyVars.viewer.getExtension("Autodesk.Viewing.MarkupsCore");
    //extension.enterEditMode();
    var data = extension.generateData();
    extension.viewer.getState();
    extension.leaveEditMode();
    //var state = extension.loadMarkups(data,MyVars.viewer);
    //extension.viewer.restorestate(state);

    // var extension = MyVars.viewer.getExtension("Autodesk.Measure");
    // extension.activate(['area'])
//     const selection = MyVars.viewer.getSelection();
//     MyVars.viewer.clearSelection();
// // Anything selected?
//     if (selection.length > 0) {
//         let isolated = [];
//         // Iterate through the list of selected dbIds
//         selection.forEach((dbId) => {
//             // Get properties of each dbId
//             MyVars.viewer.getProperties(dbId, (props) => {
//                 // Output properties to console
//                 console.log(props);
//                 // Ask if want to isolate
//                 if (confirm(`Isolate ${props.name} (${props.externalId})?`)) {
//                     isolated.push(dbId);
//                     MyVars.viewer.isolate(isolated);
//                 }
//             });
//         });
//     } else {
//         // If nothing selected, restore
//         MyVars.viewer.isolate(0);
//     }
}

function pan(){
    var extension = MyVars.viewer.getExtension('Autodesk.DefaultTools.NavTools');
    extension.activate('pan');
}
function pivot() {
    var extension = MyVars.viewer.getExtension('Autodesk.Viewing.ViewingUtilities');
    //extension.setPivotColor("0xFF0000");
}
function showDesignExplorer(document)
{
    var viewableItems = Autodesk.Viewing.Document.getSubItemsWithProperties(document.getRootItem(), {'type':'folder','role':'viewable'}, true);
    var root = viewableItems[0];
    var geometryItems = Autodesk.Viewing.Document.getSubItemsWithProperties(root, {'type':'geometry'}, true);
    if (geometryItems.length === 0)
        return false;

    if (geometryItems.length === 1) {
        // Check if the item has camera views.
        return modelDocument.getNumViews( geometryItems[0] ) > 1;
    }
    return true;
}
function toolBar(viewer){
    var button = new Autodesk.Viewing.UI.Button('toolArea');
    button.addClass('toolbarFusion');
    button.setToolTip('Browse Document');
    button.onClick = function (viewer) {
        console.log("Hello");
        //viewer.getExtension('Autodesk.DocumentBrowser');
        //$("forgeProperties").line(190, 225, 352, 225);
    };
    var button1 = new Autodesk.Viewing.UI.Button('toolArea');
    button1.addClass('toolbarFusion');
    button1.setToolTip('toolBar');
    // var tool = new Autodesk.Viewing.UI.ControlGroup('toolId');
    // tool.addControl(button);
    //tool.getControl('areaFusion')
    var toolbar = new Autodesk.Viewing.UI.ControlGroup('ToolbarAppGroup');
    toolbar.addControl(button);
    toolbar.addControl(button1);


    if (viewer.toolbar) {
        viewer.toolbar.addControl(toolbar);
    } else {
        viewer.addEventListener(Autodesk.Viewing.TOOLBAR_CREATED_EVENT, function() {
            viewer.toolbar.addControl(toolbar);
        });
    }

}

function zoomBar(){
    var extension = MyVars.viewer.getExtension('Autodesk.Viewing.ZoomWindow');
    extension.activate();
    //active(extension);
}

function active(){
    var extension = MyVars.viewer.getExtension('Autodesk.Viewing.ZoomWindow');
    extension.activate();
}
// function rollBar(){
//     var extension = MyVars.viewer.getExtension('Autodesk.BIM360.RollCamera');
//     //extension.load();
//     extension.roll();
// }
function layers(){
    var extension = MyVars.viewer.getExtension('Autodesk.LayerManager');
    extension.activate();
}
function browseModel(){
    //initializeViewer();
   var extension = MyVars.viewer.getExtension('Autodesk.DocumentBrowser');
    // extension.loadNextModel().then(()=>{
    //     initializeViewer();
    // });
     extension.activate();
}
function addSelectionListener(viewer) {
    viewer.addEventListener(
        Autodesk.Viewing.SELECTION_CHANGED_EVENT,
        function (event) {
            selectInHierarchyTree(event.dbIdArray);

            var dbId = event.dbIdArray[0];
            if (dbId) {
                viewer.getProperties(dbId, function (props) {
                   console.log(props.externalId);
                });
            }
        });
}

// Get the full path of the selected body
function getFullPath(tree, dbId) {
    var path = [];
    while (dbId) {
        var name = tree.getNodeName(dbId);
        path.unshift(name);
        dbId = tree.getNodeParentId(dbId);
    }

    // We do not care about the top 2 items because it's just the file name
    // and root component name
    path = path.splice(2, path.length - 1)

    return path.join('+');
}

function showAllProperties(viewer) {
    var instanceTree = viewer.model.getData().instanceTree;

    var allDbIds = Object.keys(instanceTree.nodeAccess.dbIdToIndex);

    for (var key in allDbIds) {
        var id = allDbIds[key];
        viewer.model.getProperties(id, function (data) {
            var str = "";
        });
    }
}

// Adds a button to the toolbar that can be used
// to check for body sepcific data in our mongo db
// Call this once the Viewer has been set up
function addFusionButton(viewer) {
    var button = new Autodesk.Viewing.UI.Button('toolbarFusion');
    button.onClick = function (e) {
        var ids = viewer.getSelection();
        if (ids.length === 1) {
            var tree = viewer.model.getInstanceTree();
            var fullPath = getFullPath(tree, ids[0]);
            console.log(fullPath);

            $.ajax ({
                url: '/dm/fusionData/' + viewer.model.loader.svfUrn + '/' + encodeURIComponent(fullPath),
                type: 'GET'
            }).done (function (data) {
                console.log('Retrieved data');
                console.log(data);

                alert(JSON.stringify(data, null, 2));
            }).fail (function (xhr, ajaxOptions, thrownError) {
                alert('Failed to retrieve data');
            }) ;
        }
    };
    button.addClass('toolbarFusionButton');
    button.setToolTip('Show Fusion properties');

    // SubToolbar
    var subToolbar = new Autodesk.Viewing.UI.ControlGroup('myFusionAppGroup');
    subToolbar.addControl(button);

    if (viewer.toolbar) {
        viewer.toolbar.addControl(subToolbar);
    } else {
        viewer.addEventListener(Autodesk.Viewing.TOOLBAR_CREATED_EVENT, function() {
            viewer.toolbar.addControl(subToolbar);
        });
    }
}
function mousePressed() {
    valueX = mouseX%255;
    valueY = mouseY%255;
}
function init(){
    ctx = document.getElementById("forgeUpload");
    dr =ctx.getContext('2d');
    $('#forgeUpload').mousedown(function (e) {
         //mousePressed = true;
        Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, false);
    });

    $('#forgeUpload').mousemove(function (e) {
        //if (mousePressed) {
            Draw(e.pageX - $(this).offset().left, e.pageY - $(this).offset().top, true);
        //}
    });

    $('#forgeUpload').mouseup(function (e) {
        //mousePressed = false;
    });
    $('#forgeUpload').mouseleave(function (e) {
        //mousePressed = false;
    });

}
function Draw(x, y, isDown) {
    lastX = x; lastY = y;
    if (isDown) {
        dr.beginPath();
        dr.strokeStyle = "#FF0000";
        dr.lineWidth = 10 ;
        dr.lineJoin = "round";
        dr.moveTo(lastX, lastY);
        dr.lineTo(x, y);
        dr.closePath();
        dr.stroke();
    }

}

function addAreaButton(viewer){
    var button = new Autodesk.Viewing.UI.Button('areaFusion',true);
    button.addClass('areaFusionButton');
    button.setToolTip('Areas');
    button.setCollapsed(true);
    button.onClick = function () {
        console.log("Hello");
        const selection = viewer.getSelection();
        viewer.loadEx
        viewer.clearSelection();
// Anything selected?
        if (selection.length > 0) {
            let isolated = [];
            // Iterate through the list of selected dbIds
            selection.forEach((dbId) => {
                // Get properties of each dbId
                viewer.getProperties(dbId, (props) => {
                    // Output properties to console
                    console.log(props);
                    // Ask if want to isolate
                    if (confirm(`Isolate ${props.name} (${props.externalId})?`)) {
                        isolated.push(dbId);
                        viewer.isolate(isolated);
                        alert("Count of "+props.name+" : "+isolated.length);
                    }
                });
            });
        } else {
            // If nothing selected, restore
            viewer.isolate(0);
        }
    };
    // SubToolbar
    var subToolbar = new Autodesk.Viewing.UI.ControlGroup('areaFusionAppGroup');
    subToolbar.addControl(button);

    if (viewer.toolbar) {
        viewer.toolbar.addControl(subToolbar);
    } else {
        viewer.addEventListener(Autodesk.Viewing.TOOLBAR_CREATED_EVENT, function() {
            viewer.toolbar.addControl(subToolbar);
        });
    }
}
function navToolbar(){
    //MyVars.viewer.loadExtension(Autodesk.Viewing.ZoomWindow);
    //var extension = MyVars.viewer.getExtension("Autodesk.Viewing.MarkupsCore");
    //extension.enterEditMode();
    var extension = MyVars.viewer.getExtension("Autodesk.Measure");
    extension.activate(['area'])
    // const selection = MyVars.viewer.getSelection();
    // MyVars.viewer.clearSelection();
// Anything selected?
//     if (selection.length > 0) {
//         let isolated = [];
//         // Iterate through the list of selected dbIds
//         selection.forEach((dbId) => {
//             // Get properties of each dbId
//             MyVars.viewer.getProperties(dbId, (props) => {
//                 // Output properties to console
//                 console.log(props);
//                 // Ask if want to isolate
//                 if (confirm(`Isolate ${props.name} (${props.externalId})?`)) {
//                     isolated.push(dbId);
//                     MyVars.viewer.isolate(isolated);
//                 }
//             });
//         });
//     } else {
//         // If nothing selected, restore
//         MyVars.viewer.isolate(0);
//     }

}

function loadDocument(viewer, documentId) {
    // Set the Environment to "Riverbank"
    viewer.setLightPreset(8);

    //this.urn_model1 ="dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6d2lwLmRtLnByb2QvYzc4MDhhNmMtN2MwZi00NzA5LTk2NzEtYTY5YjM4YmI1ZGEwLmR3Zw";
    this.urn_model1 ="house_3.dwg";
    //this.urn_model2 ="dXJuOmFkc2sub2JqZWN0czpvcy5vYmplY3Q6d2lwLmRtLnByb2QvOTEyYTAxYjItZmI5ZS00MTM0LWJiNjYtMmJjNmJjMTM0NjM3LmR3Zw";
    this.urn_model2 ="football_stadium_.dwg";


//model info array
    this.modelArray = [	{modelName:'urn_model1',urn:urn_model1,modelObj:null},
        {modelName:'urn_model2',urn:urn_model2,modelObj:null}]

    // Make sure that the loaded document's setting won't
    // override it and change it to something else
    viewer.prefs.tag('ignore-producer');

    Autodesk.Viewing.Document.load(
        documentId,
        // onLoad
        function (doc) {
            const node = doc.getRoot().getDefaultGeometry();
            if (node) {
                viewer.loadDocumentNode(doc, node);
                //showDesignExplorer(doc);
                addFusionButton(viewer);
                addAreaButton(viewer);
                toolBar(viewer);
                //viewer._model.loadNextModel();
                // var viewableItems = Autodesk.Viewing.Document.getSubItemsWithProperties(doc.getRootItem(), {'type':'folder','role':'viewable'}, true);
                // var root = viewableItems[0];
                // var geometryItems = Autodesk.Viewing.Document.getSubItemsWithProperties(root, {'type':'geometry'}, true);
                // if (geometryItems.length === 0)
                //     return false;
                //
                // if (geometryItems.length === 1) {
                //     // Check if the item has camera views.
                //     return modelDocument.getNumViews( geometryItems[0] ) > 1;
                // }
                // return true;
            }
        },
        // onError
        function (errorMsg) {
            //showThumbnail(documentId.substr(4, documentId.length - 1));
        }
    )
}

function selectInViewer(objectIds) {
    if (MyVars.viewer) {
        MyVars.viewer.select(objectIds);
    }
}

/////////////////////////////////////////////////////////////////
// Other functions
/////////////////////////////////////////////////////////////////

function showProgress(text, status) {
    var progressInfo = $('#progressInfo');
    var progressInfoText = $('#progressInfoText');
    var progressInfoIcon = $('#progressInfoIcon');

    var oldClasses = progressInfo.attr('class');
    var newClasses = "";
    var newText = text;

    if (status === 'failed') {
        newClasses = 'btn btn-danger';
    } else if (status === 'inprogress' || status === 'pending') {
        newClasses = 'btn btn-warning';
        newText += " (Click to stop)";
    } else if (status === 'success') {
        newClasses = 'btn btn-success';
    } else {
        newClasses = 'btn btn-info';
    }

    // Only update if changed
    if (progressInfoText.text() !== newText) {
        progressInfoText.text(newText);
    }

    if (oldClasses !== newClasses) {
        progressInfo.attr('class', newClasses);

        if (newClasses === 'btn btn-warning') {
            progressInfoIcon.attr('class', 'glyphicon glyphicon-refresh glyphicon-spin');
        } else {
            progressInfoIcon.attr('class', '');
        }
    }
}


